
--basic cleanup

arena_lib.on_quit('wormball', function(arena, p_name, is_spectator, reason)
    if reason ~= 0 then return end

    local player = core.get_player_by_name(p_name)
    if player and not is_spectator then
        player:set_properties({textures = wormball.player_texture_save[p_name]})
        local att = player:get_attach()
        player:set_detach()
        if att then att:remove() end
    end
end)
