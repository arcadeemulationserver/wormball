-- scoreboards
local storage = wormball.storage
local S = core.get_translator("wormball")

arena_lib.on_celebration('wormball', function(arena, winner_name)

    --reset player textures back to the texture they were... (wormball sets player textures to clear)

    for pl_name,stats in pairs(arena.players) do
        if not(arena_lib.is_player_spectating(pl_name)) then
            wormball.detach(pl_name)
        end
    end
    
    --for now, arena_lib only supports single winners... change this when it supports multiple winners

    if type(winner_name) == 'string' then


        if arena.mode == 'singleplayer' then

            -- leaderboard will be a table with the following format 
            -- {
            --    {score,plname}
            --    {score,plname}
            --   }

            -- it will always be ordered, with the highest score first.

            local new_highscore = false

            local leaderboard = wormball.leaderboard.get_highscores(arena.name) --arena.singleplayer_leaderboard

            local score = arena.players [ winner_name ] .score

            if not leaderboard then leaderboard = {} end

            if # leaderboard == 0 then
                leaderboard [ 1 ] = { score , winner_name }
                new_highscore = true

            else

                local insert = true

                for idx , winner_table in ipairs( leaderboard ) do

                    if winner_table [ 2 ] == winner_name then --if the player already has a score on the board...

                        if score > winner_table[1] then -- new higher score achieved... we will now remove the old score so we can input the new score at the correct location
                            table.remove(leaderboard , idx )
                        else -- the score wasn't higher, so no need to insert a new score
                            insert = false
                        end

                    end

                end

                if insert then

                    for idx , winner_table in ipairs( leaderboard ) do
                        if score > winner_table [ 1 ] then 
                            table.insert( leaderboard , idx , { score , winner_name } )
                            if idx == 1 then
                                new_highscore = true
                            end
                            break
                        end
                    end

                end
            end

            --arena_lib.change_arena_property(winner_name, 'wormball', arena.name, "singleplayer_leaderboard", leaderboard)

            --arena_lib.change_arena_property('wormball' , 'wormball' , arena.name , "singleplayer_leaderboard" , leaderboard )

            --save (update) the highscore data to disk
            arena.singleplayer_leaderboard = leaderboard

                        
            local success, msg = wormball.leaderboard.save_highscores(arena.name,leaderboard)
            --core.chat_send_all(tostring(success))
            --core.chat_send_all(msg)
                        
            --local serial = core.serialize(arena.singleplayer_leaderboard)

            --storage:set_string(arena.name .. "_highscores", serial )






            if new_highscore then
                arena_lib.HUD_send_msg_all("title", arena, S("New High Score!"), 2 ,'sumo_win',0xAEAE00)
                wormball.award(winner_name, "wormball:coil_king")
            else
                arena_lib.HUD_send_msg_all("title", arena, S('Game Over!'), 2 ,'sumo_win',0xAEAE00)
            end

            core.after( 3 , function( arena , score , leaderboard ) 

                arena_lib.HUD_send_msg_all("title", arena , S("You had @1 pts", score).."!", 3 ,'sumo_win',0xAEAE00)

                

                core.after( 3 , function( arena , score , leaderboard ) 

                    for pl_name , stats in pairs(arena.players_and_spectators) do
                        wormball.show_singleplayer_leaderboard( arena.name , pl_name )
                    end
                    
                end , arena , score , leaderboard )

                

            end , arena , score , leaderboard )

        else --arena.mode == 'multiplayer'

            arena_lib.HUD_send_msg_all("title", arena, S("New High Score!"), 2 ,'sumo_win',0xAEAE00)

            table.insert( arena.multi_scores , 1 , { arena.players[ winner_name ] .score , winner_name } ) 

            local scores = arena.multi_scores

            -- add some multiplayer achievements
            local len = 0
            for j,k in pairs(scores) do
                len = len + 1
            end
            if len >=4 and arena.players[ winner_name ] .score > 10 then
                wormball.award(winner_name, "wormball:global_worming")
            end

            core.after( 2 , function( arena , scores ) 
                
                for pl_name , stats in pairs(arena.players_and_spectators) do
                    wormball.show_multi_scores( arena , pl_name , scores )
                end
                    
                -- show the scoreboard here
                
            end , arena , scores )

        end

    end

end)


