wormball = {}
wormball.HUD = {}
dofile(core.get_modpath("wormball") .. "/globals.lua")

wormball.register_achievements = core.settings:get_bool("wormball_register_achievements") or false
wormball.debug = core.settings:get_bool("wormball_debug") or false


arena_lib.register_minigame("wormball", {
    name = "Wormball",
    icon = "magiccompass_wormball.png",
    chat_settings = {prefix_all = "[WB]"},
    player_aspect = {
      visual = "sprite",
      textures = {"blank.png"},
    },
    hotbar = {
      slots = 0,
      background_image = "blank.png",
      selected_image = "blank.png",
    },
    min_players = 1,
    max_players = 10,
    spectate_mode = "all",
    disable_inventory = true,
    keep_attachments = false,
    show_nametags = true,
    time_mode = 'incremental',
    load_time = 5,
    celebration_time = 10,
    in_game_physics = {
      speed = 1,
      jump = 1,
      sneak = false,
      gravity = 1,
    },
    disabled_damage_types = {"punch","fall"},
    properties = {
      min_food_factor = 2,
      min_food = 20,
      singleplayer_leaderboard = {},
      multi_scores = {},
    },

    temp_properties = {
      mode = 'singleplayer',
      dots = {},
      num_players = 0,
      time_to_next_elim = 60,
      elims = 0,
    },

    initial_time = 300,

    player_properties = {
      alive = true,
      direction = {x=0,y=1,z=0},
      old_direction = {x=0,y=1,z=0},
      nodes = {},
      score = 1,
      color = "",
      move = true,
      attached = false,
      eliminated = false,
    },
})


--nodes includes the attachment entity, also there are creative decorative worm body parts for decorating minigame hubs
dofile(core.get_modpath("wormball") .. "/nodes.lua")
dofile(core.get_modpath("wormball") .. "/privs.lua")
dofile(core.get_modpath("wormball") .. "/leaderboard.lua")
dofile(core.get_modpath("wormball") .. "/commands.lua")
--minigame_manager simply runs all the files in the folder minigame_manager
dofile(core.get_modpath("wormball") .. "/minigame_manager.lua")


function wormball.award(p_name, ach_name) return end

if wormball.register_achievements then
    dofile(core.get_modpath("wormball") .. "/achievements.lua")
end